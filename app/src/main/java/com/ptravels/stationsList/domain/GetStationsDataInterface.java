package com.ptravels.stationsList.domain;

import com.ptravels.stationsList.data.model.response.StationsListResponse;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface GetStationsDataInterface {
    public Single<StationsListResponse> getStations();
}
