package com.ptravels.stationsList.presentation;

import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.global.Utils;
import com.ptravels.stationsList.data.model.response.PathParamRequest;
import com.ptravels.stationsList.data.model.response.StationsListResponse;
import com.ptravels.stationsList.domain.GetStationsUseCase;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class StationsListPresenterImpl implements StationsListPresenter {
    private GetStationsUseCase getStationsUseCase;
    private StationListView stationListView;

    public StationsListPresenterImpl(StationListView stationListView, GetStationsUseCase getStationsUseCase) {
        this.stationListView = stationListView;
        this.getStationsUseCase = getStationsUseCase;
    }

    @Override
    public void getStationsList(PathParamRequest request) {
        if (Utils.isOnline()) {
            stationListView.showLoading();
            getStationsUseCase.execute(request)
                    .subscribe(new SingleSubscriber<StationsListResponse>() {

                        @Override
                        public void onSuccess(StationsListResponse value) {
                            stationListView.hideLoading();
                            stationListView.showStationListResults(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            stationListView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    stationListView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    stationListView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    stationListView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                stationListView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            stationListView.showError(Constants.NO_INTERNET);
        }
    }
}
