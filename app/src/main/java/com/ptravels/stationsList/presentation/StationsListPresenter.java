package com.ptravels.stationsList.presentation;


import com.ptravels.stationsList.data.model.response.PathParamRequest;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface StationsListPresenter {
    public void getStationsList(PathParamRequest request);
}
