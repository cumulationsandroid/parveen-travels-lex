package com.ptravels;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Anirudh Uppunda on 18/4/17.
 */

public class ParveenApp extends MultiDexApplication {

    private static ParveenApp mContext;

    public static Context getContext() {
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Fabric.with(this, new Crashlytics());
        mContext = this;
//        Utils.printHashKey(mContext);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static String getAccessToken(){
        return SharedPrefUtils.getSharedPrefUtils().getAccessToken(mContext);
    }
}
