package com.ptravels.REST;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Single;

/**
 * Created by Amit Tumkur on 10-12-2017.
 */

public interface MockApiService {
    @GET("cgkFNDQuJK?indent=2")
    Single<StationsListResponse> getStations();

    @GET("bUvVonRGJK?indent=2")
    Single<BusSearchResponse> getBusSearchResults();
}
