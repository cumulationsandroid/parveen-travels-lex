package com.ptravels.REST;

import android.util.Log;

import com.google.gson.Gson;
import com.ptravels.BuildConfig;
import com.ptravels.ParveenApp;
import com.ptravels.global.Constants;
import com.ptravels.global.FieldErrors;
import com.ptravels.global.ResponseError;
import com.ptravels.global.ResponseErrorBody;
import com.ptravels.global.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Anirudh Uppunda on 3/9/17.
 */

public class RestClient {
    public static boolean enableMockApi;
    static public Retrofit retrofit;

    private RestClient() {

    }

    static Retrofit getInstance() {
        return retrofit;
    }

    public static ApiService getApiService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        else logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(1,TimeUnit.MINUTES)
                .readTimeout(2,TimeUnit.MINUTES)
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(logging)
                .cache(provideCache())
                .build();

        if (enableMockApi) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.PRODUCTION_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(new MockClient(ParveenApp.getContext()))
                    .build();
        } else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.PRODUCTION_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit.create(ApiService.class);
    }

    public static MockApiService getMockApiService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        else logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(logging)
                .cache(provideCache())
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.DEMO_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit.create(MockApiService.class);
    }

    private static Cache provideCache () {
        Cache cache = null;
        try
        {
            cache = new Cache( new File(ParveenApp.getContext().getCacheDir(), "http-cache" ),
                    10 * 1024 * 1024 ); // 10 MB
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e( "OKHTTP", "Could not create Cache!" );
        }
        return cache;
    }

    public static Interceptor provideCacheInterceptor ()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Response response = chain.proceed( chain.request() );

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge( 2, TimeUnit.MINUTES )
                        .build();

                return response.newBuilder()
                        .header( "Cache-Control", cacheControl.toString() )
                        .removeHeader("Pragma")
                        .build();
            }
        };
    }

    public static Interceptor provideOfflineCacheInterceptor ()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Request request = chain.request();

                if ( !Utils.isOnline() )
                {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale( 7, TimeUnit.DAYS )
                            .build();

                    request = request.newBuilder()
                            .cacheControl( cacheControl )
                            .removeHeader("Pragma")
                            .build();
                }

                return chain.proceed( request );
            }
        };
    }

    public static String getErrorMessage(ResponseBody responseBody) {
        try {
            String errorMsg = "";
            JSONObject jsonObject = new JSONObject(responseBody.string());
            ResponseErrorBody errorBody = new Gson().fromJson(jsonObject.toString(),ResponseErrorBody.class);
            if (errorBody.getError().getGlobalErrors()!=null) {
                List<String> errors = errorBody.getError().getGlobalErrors();
                for (int i = 0; i < errors.size(); i++) {
                    if (i == 0 || i == errors.size() - 1) {
                        errorMsg = errorMsg + errors.get(i);
                    } else {
                        errorMsg = errorMsg + errors.get(i) + ", ";
                    }
                }
            } else {
                if (errorBody.getError().getFieldErrors()!=null){
                    List<FieldErrors> fieldErrorsList = errorBody.getError().getFieldErrors();
                    for (int i = 0; i < fieldErrorsList.size(); i++) {
                        if (i == 0 || i == fieldErrorsList.size() - 1) {
                            errorMsg = errorMsg + fieldErrorsList.get(i).getMessage();
                        } else {
                            errorMsg = errorMsg + fieldErrorsList.get(i).getMessage() + ", ";
                        }
                    }
                }
            }
            return errorMsg;
        } catch (Exception e) {
            e.printStackTrace();
            return "Something went wrong! Please Try again later";
        }
    }

    public static String get200StatusErrors(ResponseError error){
        if (error == null)
            return "Unknown error, try again later";
        String errMsg = "";
        if (error.getGlobalErrors()!=null && error.getGlobalErrors().size()>0) {
            for (int i = 0; i < error.getGlobalErrors().size(); i++) {
                if (i == 0 || i == error.getGlobalErrors().size() - 1) {
                    errMsg = errMsg + error.getGlobalErrors().get(i);
                } else {
                    errMsg = errMsg + error.getGlobalErrors().get(i) + ",";
                }
            }
        }

        if (error.getFieldErrors()!=null && error.getFieldErrors().size()>0) {
            for (int i = 0; i < error.getFieldErrors().size(); i++) {
                if (i == 0 || i == error.getFieldErrors().size() - 1) {
                    errMsg = errMsg + error.getFieldErrors().get(i);
                } else {
                    errMsg = errMsg + error.getFieldErrors().get(i) + ",";
                }
            }
        }
        return errMsg;
    }

}
