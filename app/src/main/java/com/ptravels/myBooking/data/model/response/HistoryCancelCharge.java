package com.ptravels.myBooking.data.model.response;

import java.io.Serializable;

public class HistoryCancelCharge implements Serializable {
	private double serviceTaxAmount;
	private double cancelCharge;

	public void setServiceTaxAmount(int serviceTaxAmount){
		this.serviceTaxAmount = serviceTaxAmount;
	}

	public double getServiceTaxAmount(){
		return serviceTaxAmount;
	}

	public void setCancelCharge(int cancelCharge){
		this.cancelCharge = cancelCharge;
	}

	public double getCancelCharge(){
		return cancelCharge;
	}

	@Override
 	public String toString(){
		return 
			"CancelChargesItem{" + 
			"serviceTaxAmount = '" + serviceTaxAmount + '\'' + 
			",cancelCharge = '" + cancelCharge + '\'' + 
			"}";
		}
}
