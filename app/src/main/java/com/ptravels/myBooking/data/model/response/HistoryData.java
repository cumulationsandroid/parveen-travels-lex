package com.ptravels.myBooking.data.model.response;

import java.util.List;

public class HistoryData {
	private List<HistoryTicket> tickets;

	public void setTickets(List<HistoryTicket> tickets){
		this.tickets = tickets;
	}

	public List<HistoryTicket> getTickets(){
		return tickets;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"tickets = '" + tickets + '\'' + 
			"}";
		}
}