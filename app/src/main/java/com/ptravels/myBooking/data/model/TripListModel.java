package com.ptravels.myBooking.data.model;

import com.ptravels.myBooking.data.model.response.HistoryTicket;
import com.ptravels.myBooking.presentation.MyBookingTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cumulations on 3/1/18.
 */

public class TripListModel implements Serializable
{
    private List<HistoryTicket> tripList=new ArrayList<>();
    private MyBookingTypes tripListType;

    public MyBookingTypes getTripListType() {
        return tripListType;
    }

    public void setTripListType(MyBookingTypes tripListType) {
        this.tripListType = tripListType;
    }

    public List<HistoryTicket> getTripList() {
        return tripList;
    }

    public void setTripList(List<HistoryTicket> tripList) {
        this.tripList = tripList;
    }
}
