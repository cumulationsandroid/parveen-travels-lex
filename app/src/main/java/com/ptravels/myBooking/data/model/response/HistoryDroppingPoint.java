package com.ptravels.myBooking.data.model.response;

import java.io.Serializable;

public class HistoryDroppingPoint implements Serializable {
	private String contactPersonName;
	private String mobileNumber;
	private String pointName;
	private String stationPointName;
	private double latitude;
	private String addressLine1;
	private String addressLandMark;
	private String addressLine2;
	private String addressPinCode;
	private double longitude;

	public void setContactPersonName(String contactPersonName){
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonName(){
		return contactPersonName;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setPointName(String pointName){
		this.pointName = pointName;
	}

	public String getPointName(){
		return pointName;
	}

	public void setStationPointName(String stationPointName){
		this.stationPointName = stationPointName;
	}

	public String getStationPointName(){
		return stationPointName;
	}

	public void setLatitude(int latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setAddressLine1(String addressLine1){
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine1(){
		return addressLine1;
	}

	public void setAddressLandMark(String addressLandMark){
		this.addressLandMark = addressLandMark;
	}

	public String getAddressLandMark(){
		return addressLandMark;
	}

	public void setAddressLine2(String addressLine2){
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine2(){
		return addressLine2;
	}

	public void setAddressPinCode(String addressPinCode){
		this.addressPinCode = addressPinCode;
	}

	public String getAddressPinCode(){
		return addressPinCode;
	}

	public void setLongitude(int longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"DroppingPoint{" + 
			"contactPersonName = '" + contactPersonName + '\'' + 
			",mobileNumber = '" + mobileNumber + '\'' + 
			",pointName = '" + pointName + '\'' + 
			",stationPointName = '" + stationPointName + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",addressLine1 = '" + addressLine1 + '\'' + 
			",addressLandMark = '" + addressLandMark + '\'' + 
			",addressLine2 = '" + addressLine2 + '\'' + 
			",addressPinCode = '" + addressPinCode + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}
