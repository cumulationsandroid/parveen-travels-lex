package com.ptravels.myBooking.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.myBooking.domain.MyBookingDataInterface;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingDataSourceImpl implements MyBookingDataInterface {
    @Override
    public Single<HistoryResponse> getMyBookingList(MyBookingRequest myBookingRequest) {

        return RestClient.getApiService().getMyBookingDetails(ParveenApp.getAccessToken(),myBookingRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        /*RestClient.enableMockApi = true;
        return RestClient.getApiService().getMockMyBookingDetails(myBookingRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());*/
    }

    @Override
    public Single<CancelTicketResponse> cancelTicket(CancelTicketRequest cancelTicketRequest) {
        return RestClient.getApiService().cancelTicket(ParveenApp.getAccessToken(),cancelTicketRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TicketDetailResponse> getTicketDetails(PathParamRequest request) {
        return RestClient.getApiService().getTicketDetails(request.getToken(),request.getPnr())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
