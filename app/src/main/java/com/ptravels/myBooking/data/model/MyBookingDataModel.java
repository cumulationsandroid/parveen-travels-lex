package com.ptravels.myBooking.data.model;

import com.ptravels.myBooking.data.model.response.HistoryTicket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingDataModel implements Serializable {
    private List<HistoryTicket> upcomingTripDataList = new ArrayList<>();
    private List<HistoryTicket> completedTripDataList = new ArrayList<>();
    private List<HistoryTicket> cancelledTripDataList = new ArrayList<>();


    public List<HistoryTicket> getCancelledTripDataList() {
        return cancelledTripDataList;
    }

    public void setCancelledTripDataList(List<HistoryTicket> cancelledTripDataList) {
        this.cancelledTripDataList = cancelledTripDataList;
    }

    public List<HistoryTicket> getCompletedTripDataList() {
        return completedTripDataList;
    }

    public void setCompletedTripDataList(List<HistoryTicket> completedTripDataList) {
        this.completedTripDataList = completedTripDataList;
    }

    public List<HistoryTicket> getUpcomingTripDataList() {
        return upcomingTripDataList;
    }

    public void setUpcomingTripDataList(List<HistoryTicket> upcomingTripDataList) {
        this.upcomingTripDataList = upcomingTripDataList;
    }
}
