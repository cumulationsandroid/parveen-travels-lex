package com.ptravels.myBooking.presentation;

import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.global.Utils;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.myBooking.domain.CancelTicketUseCase;
import com.ptravels.myBooking.domain.MyBookingUseCase;
import com.ptravels.myBooking.domain.TicketDetailUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingPresenterImpl implements MyBookingPresenter {

    private MyBookingView myBookingView;
    private MyBookingUseCase myBookingUseCase;

    public MyBookingPresenterImpl(MyBookingView myBookingView) {
        this.myBookingView = myBookingView;
    }

    public MyBookingPresenterImpl(MyBookingUseCase myBookingUseCase, MyBookingView myBookingView) {
        this.myBookingUseCase = myBookingUseCase;
        this.myBookingView = myBookingView;
    }

    @Override
    public void getMyBookingResponse(MyBookingRequest myBookingRequest) {
        if (Utils.isOnline()) {
            myBookingView.showLoading();
            myBookingUseCase.execute(myBookingRequest)
                    .subscribe(new SingleSubscriber<HistoryResponse>() {
                        @Override
                        public void onSuccess(HistoryResponse value) {
                            myBookingView.hideLoading();
                            myBookingView.updateMyBookingData(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            myBookingView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    myBookingView.showError(RestClient.getErrorMessage(responseBody));
                                    return;
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    myBookingView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    myBookingView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                myBookingView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            myBookingView.showError(Constants.NO_INTERNET);
        }
    }


    @Override
    public void cancelTicket(CancelTicketUseCase cancelTicketUseCase, final CancelTicketRequest cancelTicketRequest) {
        if (Utils.isOnline()) {
            myBookingView.showLoading();
            cancelTicketUseCase.execute(cancelTicketRequest)
                    .subscribe(new SingleSubscriber<CancelTicketResponse>() {
                        @Override
                        public void onSuccess(CancelTicketResponse value) {
                            myBookingView.hideLoading();
                            myBookingView.updateCanelTicketUi(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            myBookingView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    myBookingView.showError(RestClient.getErrorMessage(responseBody));
                                    return;
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    myBookingView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    myBookingView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                myBookingView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            myBookingView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getTicketDetails(TicketDetailUseCase ticketDetailUseCase, PathParamRequest request) {
        if (Utils.isOnline()) {
            myBookingView.showLoading();
            ticketDetailUseCase.execute(request)
                    .subscribe(new SingleSubscriber<TicketDetailResponse>() {
                        @Override
                        public void onSuccess(TicketDetailResponse value) {
                            myBookingView.hideLoading();
                            myBookingView.provideTicketDetails(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            myBookingView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    myBookingView.showError(RestClient.getErrorMessage(responseBody));
                                    return;
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    myBookingView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    myBookingView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                myBookingView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            myBookingView.showError(Constants.NO_INTERNET);
        }
    }
}
