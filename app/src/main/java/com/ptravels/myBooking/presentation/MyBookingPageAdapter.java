package com.ptravels.myBooking.presentation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.myBooking.data.model.MyBookingDataModel;
import com.ptravels.myBooking.data.model.TripListModel;
import com.ptravels.myBooking.data.model.response.HistoryTicket;

import java.util.ArrayList;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingPageAdapter extends FragmentStatePagerAdapter {
    private final int TOTAl_PAGE = MyBookingTypes.values().length;
    private FragmentManager fragmentManager;
    private MyBookingDataModel myBookingDataModel;
    private Context context;

    public void updateList(MyBookingDataModel myBookingDataModel) {
        this.myBookingDataModel = myBookingDataModel;
    }

    public MyBookingDataModel getData() {
        return myBookingDataModel;
    }

    public MyBookingPageAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.fragmentManager = fragmentManager;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return getFragmentForPosition(position);
    }

    @Override
    public int getCount() {
        return myBookingDataModel != null ? TOTAl_PAGE : 0;
    }

    private Fragment getFragmentForPosition(int pagePostion) {
        ArrayList<HistoryTicket> list = null;

        switch (MyBookingTypes.getPageStringIdForPage(pagePostion)) {
            case R.string.upcoming:
                list = (ArrayList<HistoryTicket>) myBookingDataModel.getUpcomingTripDataList();
                break;
            case R.string.completed:
                list = (ArrayList<HistoryTicket>) myBookingDataModel.getCompletedTripDataList();
                break;
            case R.string.cancelled:
                list = (ArrayList<HistoryTicket>) myBookingDataModel.getCancelledTripDataList();
                break;
        }
        TripListModel tripListModel = new TripListModel();
        tripListModel.setTripList(list);
        tripListModel.setTripListType(MyBookingTypes.getTypeForPosition(pagePostion));

        MyBookingFragment myBookingFragment = new MyBookingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.MY_BOOKING_DATA, tripListModel);
        myBookingFragment.setArguments(bundle);

        return myBookingFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(MyBookingTypes.getPageStringIdForPage(position));
    }

    /*@Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/
}
