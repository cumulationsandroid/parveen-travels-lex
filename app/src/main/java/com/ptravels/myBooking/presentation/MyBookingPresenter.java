package com.ptravels.myBooking.presentation;

import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.domain.CancelTicketUseCase;
import com.ptravels.myBooking.domain.TicketDetailUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public interface MyBookingPresenter
{
    void getMyBookingResponse(MyBookingRequest myBookingRequest);
    void cancelTicket(CancelTicketUseCase cancelTicketUseCase,CancelTicketRequest cancelTicketRequest);
    void getTicketDetails(TicketDetailUseCase ticketDetailUseCase, PathParamRequest request);
}
