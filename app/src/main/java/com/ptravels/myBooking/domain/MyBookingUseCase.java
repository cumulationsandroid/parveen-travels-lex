package com.ptravels.myBooking.domain;

import com.ptravels.global.UseCase;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.source.MyBookingDataSourceImpl;

import rx.Single;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingUseCase extends UseCase<MyBookingRequest,HistoryResponse>
{
    private MyBookingDataSourceImpl myBookingDataSource=new MyBookingDataSourceImpl();

    @Override
    public Single<HistoryResponse> buildUseCase(MyBookingRequest myBookingRequest) {
        return myBookingDataSource.getMyBookingList(myBookingRequest);
    }
}
