package com.ptravels.global;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Constants {
    public final static String BASE_URL_OLD = "http://192.168.0.50:1000/parveen-travels/api/app/";
    public final static String BASE_URL = "http://111.93.236.131:8080/parveen-travels/api/app/";
    public final static String PRODUCTION_BASE_URL = "https://parveentravels.com/parveen-travels/api/app/";
    public static final String DEMO_BASE_URL = "http://json-generator.appspot.com/api/json/get/";
    public static final String FROM_STATION_ID = "fromStationId";
    public static final String TO_STATION_ID = "toStationId";
    public static final String TRAVEL_DATE = "travelDate";
    public static final String EMAIL_ID = "emailId";
    public static final String USER_PREF = "userPref";
    public static final String FNAME = "fname";
    public static final String USERNAME = "username";
    public static final String MOBILE = "mobile";
    public static final String PWD = "pwd";
    public static final String ENABLED = "enabled";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String USER_DATA = "userData";
    public static final String FROM_STATION_NAME = "fromStationName";
    public static final String TO_STATION_NAME = "toStationName";

    public static final String GUEST_USER_EMAIL = "guest@parveentravels.com";
    public static final String GUEST_USER_PASSWORD = "123456";
    //    public static final String GUEST_USER_EMAIL = "amit.test@test.com";
//    public static final String GUEST_USER_PASSWORD = "password1";
    public static final String FILTERS_LIST = "filtersList";
    public static final String TICKET_BOOKED = "BOOKED";
    public static final String TICKET_CANCELLED = "CANCELLED";
    public static final String MY_BOOKING_DATA = "my_booking_data";
    public static final String STATION_ID = "stationID";
    public static final String SCHEDULE_ID = "scheduleId";
    public static final String STEERING = "steering";
    public static final String SEATMAP_MODEL = "seatMapModel";
    public static final String BOOKED_SEAT_DATA = "booked_seat_data";
    public static final String BUS_TYPE = "bus_type";

    public static final String DEFAULT_PASSWORD = "123456";
    public static final String TICKET_PREF = "ticketPref";
    public static final String SINGLE_TRIP_TICKET = "singleTripTicket";

    public static final String PRE_SELECTED = "isStationPreSelected";
    public static final String paytm_callback_url = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
//    public static final String paytm_production_callback_url = "https://secure.paytm.in/paytmchecksum/paytmCallback.jsp";

    public static final String paytm_production_callback_url = "https://securegw.paytm.in/theia/paytmCallback?";
    public static final String paytm_production_callback_url_2 = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    public static final String NO_INTERNET = "Please check your internet";
    public static final String NETWORK_ERROR="Network error. please try again later";
    public static final String GUEST_USER_LOGIN = "GUEST_USER_LOGIN";
    public static final String SIGN_IN_DATA = "sign_in_data";
    public static final String DEFAULT_MOBILE_NUMBER = "9999999999";

    public static final String HOME_SCREEN_IMAGE = "https://www.parveentravels.com/parveen-travels/assets/parveen/images/new/home/02_02.jpg";
    public static final String NETWORK_CHANGED = "networkChanged";
    public static final String BUS_STATION_RESPONSE="bus_station_response";
    public static final String LOAD_FRAGMENT = "loadFragment";
    public static final String UNKNOWN_ERROR="Something went wrong! Please Try again later";
}
