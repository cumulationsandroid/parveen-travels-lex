package com.ptravels.global;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

import java.lang.reflect.Type;

/**
 * Created by Amit Tumkur on 15-12-2017.
 */

public class SharedPrefUtils {
    private static SharedPrefUtils sharedPrefUtils;

    private SharedPrefUtils() {
    }

    public static SharedPrefUtils getSharedPrefUtils() {
        if (sharedPrefUtils == null)
            sharedPrefUtils = new SharedPrefUtils();
        return sharedPrefUtils;
    }

    public void saveUserDetails(Context context, UserData userData) {
        SharedPreferences userPref = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPref.edit();
        editor.putString(Constants.FNAME, userData.getFirstName());
        editor.putString(Constants.EMAIL_ID, userData.getEmail());
        editor.putString(Constants.USERNAME, userData.getUsername());
        editor.putString(Constants.MOBILE, userData.getMobile());
        editor.putString(Constants.PWD, userData.getPassword());
        editor.putBoolean(Constants.ENABLED, userData.isEnabled());
        editor.putString(Constants.ACCESS_TOKEN, userData.getAccessToken());
        editor.apply();
    }

    public void setGuestUserLogin(Context context, boolean state) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(Constants.GUEST_USER_LOGIN, state)
                .apply();
    }

    public boolean isGuestUserLogin(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getBoolean(Constants.GUEST_USER_LOGIN, false);
    }

    public String getUserEmailID(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.EMAIL_ID, "");
    }

    public String getMobileNumber(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.MOBILE, "");
    }

    public void saveAccessToken(Context context, String accessToken) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.ACCESS_TOKEN, accessToken)
                .apply();
    }

    public String getAccessToken(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.ACCESS_TOKEN, "");
    }

    public void clearUserData(Context context) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

    public boolean isUserLoggedIn(Context context) {
        String accessToken = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.ACCESS_TOKEN, "");
        return !accessToken.isEmpty();
    }

    public void saveSingleTripTicketData(Context context, BookedSeatModel bookedSeatModel) {
        try {

            Gson gson = new Gson();
            String bookedSeatModelJsonString = gson.toJson(bookedSeatModel);
            context.getSharedPreferences(Constants.TICKET_PREF, Context.MODE_PRIVATE)
                    .edit()
                    .putString(Constants.SINGLE_TRIP_TICKET, bookedSeatModelJsonString)
                    .apply();
            Log.d("saveTicketData", "success");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public BookedSeatModel getSingleTripTicketData(Context context) {

        try {
            Gson gson = new Gson();
            String json = context.getSharedPreferences(Constants.TICKET_PREF, Context.MODE_PRIVATE)
                    .getString(Constants.SINGLE_TRIP_TICKET, "");
            Type typ1ew2 = new TypeToken<BookedSeatModel>() {
            }.getType();
            return gson.fromJson(json, typ1ew2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void saveBusStation(Context context, StationsListResponse stationsListResponse) {
        Gson gson = new Gson();
        String json = gson.toJson(stationsListResponse);
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.BUS_STATION_RESPONSE, json)
                .apply();
    }

    public StationsListResponse getBusStation(Context context) {

        String jsonString = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.BUS_STATION_RESPONSE, "");
        if (!jsonString.isEmpty()) {
            Gson gson = new Gson();
            Type typ1ew2 = new TypeToken<StationsListResponse>() {}.getType();
            return gson.fromJson(jsonString, typ1ew2);
        }
        return null;
    }
}
