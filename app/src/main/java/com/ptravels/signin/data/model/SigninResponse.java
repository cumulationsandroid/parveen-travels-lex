package com.ptravels.signin.data.model;

import com.ptravels.global.ResponseError;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SigninResponse {
    private String data;
    private boolean success;
    private ResponseError error;

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
