package com.ptravels.signin.domain;

import com.ptravels.global.UseCase;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.data.source.SigninDataSourceImpl;

import rx.Single;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SigninUseCase extends UseCase<SigninRequest,SigninResponse> {

    private SigninDataInterface signinDataSource = new SigninDataSourceImpl();

    @Override
    public Single<SigninResponse> buildUseCase(SigninRequest request) {
        return signinDataSource.getSigninResult(request);
    }
}
