package com.ptravels.signin.domain;

import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;

import rx.Single;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SigninDataInterface {
    Single<SigninResponse> getSigninResult(SigninRequest request);
}
