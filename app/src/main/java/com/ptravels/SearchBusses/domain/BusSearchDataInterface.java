package com.ptravels.SearchBusses.domain;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BusSearchDataInterface {
    Single<BusSearchResponse> getBusSearchResults(BusSearchRequest request);
}
