package com.ptravels.SearchBusses.data.model.response;

/**
 * Created by praveenkumar on 24/01/18.
 */

public class SearchFragmentData {

    String fromStationId;
    String toStationId;

    public String getFromStationId() {
        return fromStationId;
    }

    public String getToStationId() {
        return toStationId;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public String getFromStationName() {
        return fromStationName;
    }

    public String getToStattionName() {
        return toStattionName;
    }

    String travelDate;
    String fromStationName;
    String toStattionName;

    public SearchFragmentData(String fromStationId, String toStationId,
                       String travelDate,String fromStationName, String toStattionName) {

        this.fromStationId=fromStationId;
        this.toStationId=toStationId;
        this.travelDate=travelDate;
        this.fromStationName=fromStationName;
        this.toStattionName=toStattionName;



    }




    }
