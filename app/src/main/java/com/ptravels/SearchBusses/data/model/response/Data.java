package com.ptravels.SearchBusses.data.model.response;

import java.util.List;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Data {
    private List<Schedule> schedules = null;

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }
}
