package com.ptravels.SearchBusses.data.model.response;

import com.ptravels.global.ResponseError;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BusSearchResponse {
    private Boolean success;
    private Data data;
    private ResponseError error;

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
