package com.ptravels.SearchBusses.presentation;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.SearchBusses.data.model.response.Schedule;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by cumulations on 25/7/17.
 */

public class SearchResultsAdapter extends ShimmerLoaderAdapter<Schedule> {

    private Context context;
    private int topColor ;
    private int middleColor ;
    private int bottomColor ;
    private SearchResultsFragment fragment;

    public SearchResultsAdapter(Context context, List<Schedule> scheduleList,SearchResultsFragment fragment) {
        this.context = context;
        this.fragment = fragment;
        topColor = ContextCompat.getColor(context,R.color.gradientPink);
        middleColor = ContextCompat.getColor(context,R.color.gradientPurple);
        bottomColor = ContextCompat.getColor(context,R.color.gradientBlue);
        setTheDataList(scheduleList);
    }


    @Override
    public ShimmerViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            //Calling onCreate
            Log.d("called", "onCreateViewHolderCalled" + Calendar.getInstance());
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.search_results_list_item, parent, false);
            return new RecyclerViewHolder(view);
        }


    @Override
    public void onBindViewHolder(ShimmerViewHolder recyclerViewHolder1, final int position) {

        //Dont forget to call this as this will manage visibility of our placeholder shimmer view
            super.onBindViewHolder(recyclerViewHolder1,position);
        final RecyclerViewHolder recyclerViewHolder=(RecyclerViewHolder) recyclerViewHolder1;

            if (isShimmerDataActive){
                recyclerViewHolder.parentLayout.setVisibility(View.GONE);
                return;
            } else {
                recyclerViewHolder.parentLayout.setVisibility(View.VISIBLE);
            }

            final Schedule schedule;
            schedule = getTheDataElement().get(position);

            if (schedule != null) {
                recyclerViewHolder.routeTv.setText(schedule.getRouteName());
                String[] routeParts = schedule.getRouteName().split("-");
                if (routeParts.length>1){
                    recyclerViewHolder.routeTv.setText(routeParts[0]+" - "+routeParts[1]);
                }

                /*if (schedule.getBusType().contains("Green"))
                    recyclerViewHolder.goGreenTv.setText("Go Green");
                else recyclerViewHolder.goGreenTv.setVisibility(View.GONE);*/

                if (schedule.getAmenityList()!=null && schedule.getAmenityList().size()>0){
                    if (schedule.getAmenityList().contains("Go Green")){
                        recyclerViewHolder.goGreenTv.setVisibility(View.VISIBLE);
                        recyclerViewHolder.goGreenTv.setText("Go Green");
                    } else {
                        recyclerViewHolder.goGreenTv.setVisibility(View.GONE);
                    }
                }

                Shader textShader = new LinearGradient(0, 0, 50, 50,
                        new int[]{topColor, middleColor, bottomColor},
                        new float[]{0, 1, 2}/*null*/,   /*keep null for evenly distribution for gradient line*/
                        Shader.TileMode.REPEAT);//Assumes bottom and top are colors defined above
                TextPaint textPaint = recyclerViewHolder.priceTv.getPaint();
                textPaint.setShader(textShader);

                if (schedule.getAvailableSeats().equals(0)){
                    recyclerViewHolder.priceTv.setText("SOLD OUT");
                } else {
                    String fare = String.valueOf(schedule.getFares().get(0).getFare());
                    recyclerViewHolder.priceTv.setText("\u20B9 " + fare);
                }

                recyclerViewHolder.timingTv.setText(schedule.getDepartureTime() + " \u2794 " + schedule.getArrivalTime());

                recyclerViewHolder.busInfoTv.setSelected(true);
                recyclerViewHolder.busInfoTv.setText(schedule.getBusType());
                recyclerViewHolder.availableSeatsDurationTv.setText("Seats Avl: " + String.valueOf(schedule.getAvailableSeats())
                +" | Dur: "+schedule.getDuration()+" hrs");

                /*recyclerViewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fragment.openSeatLayout(schedule.getScheduleId(), schedule.getBusType());
                    }
                });*/


                Observable<View> parentClickObservable = Observable.create(new Observable.OnSubscribe<View>() {
                    @Override
                    public void call(final Subscriber<? super View> subscriber) {
                        recyclerViewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (subscriber.isUnsubscribed())
                                    return;
                                subscriber.onNext(v);
                            }
                        });
                    }
                });

                parentClickObservable.debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<View>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(View view) {
                                fragment.openSeatLayout(schedule.getScheduleId(), schedule.getBusType());
                            }
                        });
            }

    }



    public void refreshAdapter(List<Schedule> scheduleList){
        setTheDataListWithoutShimmer(scheduleList,false);
        notifyDataSetChanged();
    }


    public static class RecyclerViewHolder extends ShimmerViewHolder {

        RelativeLayout parentLayout;
        TextView routeTv, goGreenTv, priceTv, busInfoTv, timingTv, availableSeatsDurationTv;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            parentLayout = itemView.findViewById(R.id.parentLayout);
            routeTv = (TextView) itemView.findViewById(R.id.routeTv);
            goGreenTv = (TextView) itemView.findViewById(R.id.goGreenTv);
            priceTv = (TextView) itemView.findViewById(R.id.priceTv);
            busInfoTv = (TextView) itemView.findViewById(R.id.busInfoTv);
            timingTv = (TextView) itemView.findViewById(R.id.timingInfoTv);
            availableSeatsDurationTv = (TextView) itemView.findViewById(R.id.availableSeatsDurationTv);
        }
    }
}



