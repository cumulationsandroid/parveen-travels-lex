package com.ptravels.SearchBusses.presentation;

import com.ptravels.SearchBusses.data.model.response.Schedule;

import java.util.List;

/**
 * Created by Amit Tumkur on 28-12-2017.
 */

public interface FragmentApplyFiltersListener {
    void applyFilters(List<String> filtersList);
}
