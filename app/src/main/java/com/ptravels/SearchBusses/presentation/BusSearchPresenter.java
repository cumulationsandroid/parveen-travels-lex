package com.ptravels.SearchBusses.presentation;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BusSearchPresenter {
    void getBusSearchResults(BusSearchRequest request);
}
