package com.ptravels.SearchBusses.presentation;

import com.ptravels.SearchBusses.data.model.response.BoardingPoint;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.SearchBusses.data.model.response.DroppingPoint;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BusSearchView {
    void showLoading();
    void hideLoading();
    /*void showBusSearchResults(BusSearchResponse response,
                              HashMap<Integer,BoardingPoint> boarMap,
                              HashMap<Integer,DroppingPoint> deboardingMap);*/
    void showBusSearchResults(BusSearchResponse response, List<String> brPts, List<String> drPts);
    void showError(String error);
}
