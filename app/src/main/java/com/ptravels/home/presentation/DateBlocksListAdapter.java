package com.ptravels.home.presentation;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptravels.home.data.model.NextDay;
import com.ptravels.R;

import java.util.List;

/**
 * Created by cumulations on 25/7/17.
 */

public class DateBlocksListAdapter extends RecyclerView.Adapter<DateBlocksListAdapter.MyViewHolder> {

    private Context context;
    private List<NextDay> nextDaysList;

    public DateBlocksListAdapter(Context context, List<NextDay> nextDaysList) {
        this.context = context;
        this.nextDaysList = nextDaysList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dateTv, dayTv;
        CheckableLinearLayout dateBlockLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            dateTv = (TextView) itemView.findViewById(R.id.dateText);
            dayTv = (TextView) itemView.findViewById(R.id.dayText);
            dateBlockLayout = itemView.findViewById(R.id.dateBlockLayout);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.date_blocks_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {
        final NextDay nextDay = nextDaysList.get(position);
        String nextDayValue = /*"Today, 12 Dec"*/nextDay.getNextDayText();
        String[] nextDayByComma = nextDayValue.split("\\,");
        final String dayName = nextDayByComma[0];

        String[] dateBySpace = nextDayByComma[1].trim().split("\\ ");
        final String date = dateBySpace[0];
        myViewHolder.dateTv.setText(date);
        myViewHolder.dayTv.setText(dayName);

        if (nextDay.isSelected()){
            myViewHolder.dateBlockLayout.setChecked(true);
            myViewHolder.dayTv.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            myViewHolder.dateTv.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            ((HomeActivity)context).updateSelectedDate(nextDaysList.get(position).getNextDayText());
        } else {
            myViewHolder.dateBlockLayout.setChecked(false);
            myViewHolder.dayTv.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            myViewHolder.dateTv.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        }

        myViewHolder.dateBlockLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetDaysSelection();
                if (myViewHolder.dateBlockLayout.isChecked()){
                    nextDaysList.get(position).setSelected(false);
                    notifyDataSetChanged();
                } else {
                    nextDaysList.get(position).setSelected(true);
                    notifyDataSetChanged();
                }
//                ((HomeActivity)context).updateSelectedDate(nextDaysList.get(position).getNextDayText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return nextDaysList != null ? nextDaysList.size() : 0;
    }

    private void resetDaysSelection(){
        for (int i=0;i<nextDaysList.size();i++){
            nextDaysList.get(i).setSelected(false);
        }
    }

}
