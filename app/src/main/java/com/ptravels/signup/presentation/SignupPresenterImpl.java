package com.ptravels.signup.presentation;

import com.google.gson.Gson;
import com.ptravels.global.Constants;
import com.ptravels.global.FieldErrors;
import com.ptravels.global.Utils;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.signup.domain.SignupUseCase;

import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SignupPresenterImpl implements SignupPresenter {
    private SignupUseCase signupUseCase;
    private SignupView signupView;

    public SignupPresenterImpl(SignupUseCase signupUseCase, SignupView signupView) {
        this.signupUseCase = signupUseCase;
        this.signupView = signupView;
    }

    @Override
    public void getSignupResponse(SignupRequest request) {
        if(Utils.isOnline()){
        signupUseCase.execute(request)
                .subscribe(new SingleSubscriber<SignupResponse>() {
                    @Override
                    public void onSuccess(SignupResponse value) {
                        signupView.updateSignupResponse(value);
                    }

                    @Override
                    public void onError(Throwable error) {

                        try {
                            if (error instanceof HttpException) {
                                ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                signupView.showError(getErrorMessage(responseBody));
                            } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                signupView.showError(Constants.NETWORK_ERROR);
                            } else {
                                signupView.showError(error.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            signupView.showError(Constants.UNKNOWN_ERROR);
                        }

                    }
                });}
                else {
            signupView.showError(Constants.NO_INTERNET);
        }
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {
            String errorMsg = "";
            JSONObject jsonObject = new JSONObject(responseBody.string());
            SignupResponse signupResponse = new Gson().fromJson(jsonObject.toString(),SignupResponse.class);
            if (signupResponse.getError().getGlobalErrors()!=null) {
                List<String> errors = signupResponse.getError().getGlobalErrors();
                for (int i = 0; i < errors.size(); i++) {
                    if (i == 0 || i == errors.size() - 1) {
                        errorMsg = errorMsg + errors.get(i);
                    } else {
                        errorMsg = errorMsg + errors.get(i) + ", ";
                    }
                }
            } else {
                if (signupResponse.getError().getFieldErrors()!=null){
                    List<FieldErrors> fieldErrorsList = signupResponse.getError().getFieldErrors();
                    for (int i = 0; i < fieldErrorsList.size(); i++) {
                        if (i == 0 || i == fieldErrorsList.size() - 1) {
                            errorMsg = errorMsg + fieldErrorsList.get(i).getMessage();
                        } else {
                            errorMsg = errorMsg + fieldErrorsList.get(i).getMessage() + ", ";
                        }
                    }
                }
            }

            return errorMsg;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
