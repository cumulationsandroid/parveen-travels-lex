
package com.ptravels.seatlayout.data.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BoardingPoint implements Serializable {

    @SerializedName("stationPointName")
    @Expose
    private String stationPointName;
    @SerializedName("addressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("addressLine2")
    @Expose
    private String addressLine2;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("contactPersonName")
    @Expose
    private String contactPersonName;
    @SerializedName("addressLandMark")
    @Expose
    private String addressLandMark;
    @SerializedName("day")
    @Expose
    private Integer day;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("addressPinCode")
    @Expose
    private String addressPinCode;
    @SerializedName("stationPointTime")
    @Expose
    private String stationPointTime;
    @SerializedName("stationPointId")
    @Expose
    private Integer stationPointId;

    public long stationPtTimeInMillis;

    public String getStationPointName() {
        return stationPointName;
    }

    public void setStationPointName(String stationPointName) {
        this.stationPointName = stationPointName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getAddressLandMark() {
        return addressLandMark;
    }

    public void setAddressLandMark(String addressLandMark) {
        this.addressLandMark = addressLandMark;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddressPinCode() {
        return addressPinCode;
    }

    public void setAddressPinCode(String addressPinCode) {
        this.addressPinCode = addressPinCode;
    }

    public String getStationPointTime() {
        return stationPointTime;
    }

    public void setStationPointTime(String stationPointTime) {
        this.stationPointTime = stationPointTime;
    }

    public Integer getStationPointId() {
        return stationPointId;
    }

    public void setStationPointId(Integer stationPointId) {
        this.stationPointId = stationPointId;
    }

}
