package com.ptravels.seatlayout.data.model;

import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cumulations on 29/12/17.
 */

public class SeatMapModel implements Serializable{
    private List<LayoutDetail> lowerSeatList;
    private List<LayoutDetail> upperSeatList;
    private int tireCount;
    private int noColumns;
    private int noRows;

    public int getNoColumns() {
        return noColumns;
    }

    public void setLowerSeatList(List<LayoutDetail> lowerSeatList) {
        this.lowerSeatList = lowerSeatList;
    }

    public int getNoRows() {
        return noRows;
    }

    public void setNoColumns(int noColumns) {
        this.noColumns = noColumns;
    }

    public List<LayoutDetail> getLowerSeatList() {
        return lowerSeatList;
    }

    public void setNoRows(int noRows) {
        this.noRows = noRows;
    }

    public List<LayoutDetail> getUpperSeatList() {
        return upperSeatList;
    }

    public int getTireCount() {
        return tireCount;
    }

    public void setTireCount(int tireCount) {
        this.tireCount = tireCount;
    }

    public void setUpperSeatList(List<LayoutDetail> upperSeatList) {
        this.upperSeatList = upperSeatList;
    }
}
