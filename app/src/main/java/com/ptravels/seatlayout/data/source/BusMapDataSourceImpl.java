package com.ptravels.seatlayout.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.SearchBusses.data.model.response.Schedule;
import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;
import com.ptravels.seatlayout.domain.BusMapDataSourceInterface;

import rx.Scheduler;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by praveenkumar on 14/12/17.
 */

public class BusMapDataSourceImpl implements BusMapDataSourceInterface {
    @Override
    public Single<BusMapResponse> getBusMap(BusMapRequest request) {
//        RestClient.enableMockApi = true;
//        return RestClient.getApiService().getBusMapResults(request)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io());

        return RestClient.getApiService().getBusMapLayout(ParveenApp.getAccessToken(),request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

    }
}
