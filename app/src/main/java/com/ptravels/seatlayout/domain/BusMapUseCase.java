package com.ptravels.seatlayout.domain;

import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;
import com.ptravels.global.UseCase;
import com.ptravels.seatlayout.data.source.BusMapDataSourceImpl;

import rx.Single;

/**
 * Created by praveenkumar on 14/12/17.
 */

public class BusMapUseCase extends UseCase<BusMapRequest,BusMapResponse>{

    private BusMapDataSourceInterface busDataSource = new BusMapDataSourceImpl();

    @Override
    public Single<BusMapResponse> buildUseCase(BusMapRequest requestObj) {

        return busDataSource.getBusMap(requestObj);
    }

}
