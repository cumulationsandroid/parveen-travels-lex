package com.ptravels.seatlayout.presentation;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.R;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by cumulations on 28/12/17.
 */

public class BusMapSeatAdapter extends RecyclerView.Adapter<BusMapSeatAdapter.BusMapSeatAdapterViewHolder> {

    public static final String AVAILABLE = "A";
    public static final String BOOKED = "BO";
    public static final String BLOCKED = "BL";
    public static final String AS = "AS";
    public static final String FEMALE_SEAT = "FEMALE";

    public static final String SLEEPER = "SL";
    public static final String UPPER_SLEEPER = "USL";
    public static final String LOWER_SLEEPER = "LSL";

    BusMapActivity activity;
    ArrayList<LayoutDetail> detailsArray;
    ArrayList<LayoutDetail> uppserSeatList;
    ArrayList<LayoutDetail> lowerSeatList;
    LinkedHashMap<String, LayoutDetail> selectedSeatedList;

    Layout_Type type;
    private LinearLayout.LayoutParams seatLayoutParams;
    private LinearLayout.LayoutParams sleeperLayoutParams;

    public BusMapSeatAdapter(BusMapActivity activity, Layout_Type type) {
        this.activity = activity;
        this.type = type;
        selectedSeatedList = new LinkedHashMap<>();
        float seatWidth = activity.getResources().getDimension(R.dimen.seater_width);
        float seatHeight = activity.getResources().getDimension(R.dimen.seater_height);
        float sleeperWidth = activity.getResources().getDimension(R.dimen.sleeper_width);
        float sleeperHeight = activity.getResources().getDimension(R.dimen.sleeper_height);
        int left = (int) activity.getResources().getDimension(R.dimen.seat_grid_marginLeftRight);
        int top = (int) activity.getResources().getDimension(R.dimen.seat_grid_marginTopBottom);
        seatLayoutParams = new LinearLayout.LayoutParams((int) seatWidth, (int) seatHeight);
        sleeperLayoutParams = new LinearLayout.LayoutParams((int) sleeperWidth, (int) sleeperHeight);
        sleeperLayoutParams.setMargins(left, top, left, top);
        seatLayoutParams.setMargins(left, top, left, top);
    }

    public void setUpperSeatlist(ArrayList<LayoutDetail> upperSeatList) {
        this.uppserSeatList = upperSeatList;
        detailsArray = upperSeatList;
        notifyDataSetChanged();
    }

    public void setLowerSeatList(ArrayList<LayoutDetail> lowerSeatList) {
        this.lowerSeatList = lowerSeatList;
        detailsArray = lowerSeatList;
        notifyDataSetChanged();
    }

    public void setType(Layout_Type layout_type) {
        this.type = layout_type;
    }

    @Override
    public BusMapSeatAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.adapator_seatmapview, parent, false);

        return new BusMapSeatAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BusMapSeatAdapterViewHolder holder, final int position) {
        final LayoutDetail layoutDetail = detailsArray.get(position);
        Log.d("seat", "Pos " + position + "," + layoutDetail.getSeatNumber());
        handleSingleSeater(holder, layoutDetail);

        holder.mTextView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!detailsArray.get(position).getSeatNumber().equalsIgnoreCase("-")
                        && !(layoutDetail.getStatus().equalsIgnoreCase(BOOKED)||layoutDetail.getStatus().equalsIgnoreCase(BLOCKED))) {
                    if (selectedSeatedList.containsKey(detailsArray.get(position).getSeatNumber())) {
                        selectedSeatedList.remove(detailsArray.get(position).getSeatNumber());
                    } else {
                        if (selectedSeatedList.size() < 6) {
                            selectedSeatedList.put(detailsArray.get(position).getSeatNumber(), detailsArray.get(position));
                        } else {
                            Toast.makeText(activity, R.string.seat_greater_than_6_selection_error, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    detailsArray.get(position).toggleSelection();
                    activity.updateUIForSelectedSeats(selectedSeatedList);
                    notifyItemChanged(position);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return detailsArray != null ? detailsArray.size() : 0;
    }

    private void handleSingleSeater(BusMapSeatAdapterViewHolder holder, LayoutDetail detail) {
        holder.mTextView1.setVisibility(View.VISIBLE);
        Log.d("seat", "R "+detail.getRow() + ",C " + detail.getColumn()+" "+detail.getSeatNumber());
        if (detail.getSeatType().equals(SLEEPER)
                || detail.getSeatType().equals(UPPER_SLEEPER)
                || detail.getSeatType().equals(LOWER_SLEEPER)
                || (detail.getSeatType().equals(AS) && !type.equals(Layout_Type.SINGLE_SEATER))) {

            holder.mTextView1.setLayoutParams(sleeperLayoutParams);
            if (detail.getSeatNumber().equalsIgnoreCase("-")) {
                holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_blocked);
                holder.mTextView1.setVisibility(View.INVISIBLE);
            } else if (detail.getStatus().equalsIgnoreCase(BLOCKED)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT))
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_blocked_female);
                else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_blocked);
            } else if (detail.getStatus().equalsIgnoreCase(BOOKED)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT))
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_booked_female);
                else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_blocked);
            } else if (detail.isSelected()) {
                holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_selected_bus);
            } else if (detail.getStatus().equalsIgnoreCase(AVAILABLE)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT)) {
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_available_female);
                } else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_available);
            } else {
                holder.mTextView1.setBackgroundResource(R.drawable.ic_sleeper_blocked);
                holder.mTextView1.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.mTextView1.setLayoutParams(seatLayoutParams);
            if (detail.getSeatNumber().equalsIgnoreCase("-")) {
                holder.mTextView1.setBackgroundResource(R.drawable.asientogris_trans);
            } else if (detail.getStatus().equalsIgnoreCase(BLOCKED)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT))
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_blocked_female);
                else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_blocked);
            } else if (detail.getStatus().equalsIgnoreCase(BOOKED)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT))
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_blocked_female);
                else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_blocked);
            } else if (detail.isSelected()) {
                holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_selected_bus);
            } else if (detail.getStatus().equalsIgnoreCase(AVAILABLE)) {
                if (detail.getAvailableFor().equalsIgnoreCase(FEMALE_SEAT)) {
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_available_female);
                } else
                    holder.mTextView1.setBackgroundResource(R.drawable.ic_seater_available);
            } else {
                holder.mTextView1.setBackgroundResource(R.drawable.asientogris_trans);
            }
        }
    }

    public static class BusMapSeatAdapterViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout itemviewll;
        public TextView mTextView1;

        public BusMapSeatAdapterViewHolder(View itemView) {
            super(itemView);
            mTextView1 = itemView.findViewById(R.id.gridelement);
            itemviewll = itemView.findViewById(R.id.seat_itemview);
        }
    }
}