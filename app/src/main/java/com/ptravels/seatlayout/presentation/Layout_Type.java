package com.ptravels.seatlayout.presentation;

/**
 * Created by praveenkumar on 17/12/17.
 */

public enum  Layout_Type {
    SINGLE_SEATER,
    LOWER_UPPER_SEATER,
    UPPER_SEATER,
    LOWER_SEATER,
    SLEEPER_LOWER,
    SLEEPER_UPPER
}
