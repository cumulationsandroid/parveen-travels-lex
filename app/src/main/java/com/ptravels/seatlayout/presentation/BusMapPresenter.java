package com.ptravels.seatlayout.presentation;

import com.ptravels.seatlayout.data.model.request.BusMapRequest;

/**
 * Created by praveenkumar on 15/12/17.
 */

public interface BusMapPresenter {
    void getBusMapLayout(BusMapRequest request);
}
