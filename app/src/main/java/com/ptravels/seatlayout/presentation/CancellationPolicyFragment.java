package com.ptravels.seatlayout.presentation;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.ImageView;

import com.ptravels.R;

/**
 * Created by praveenkumar on 25/12/17.
 */

public class CancellationPolicyFragment extends BottomSheetDialogFragment {


    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.fragment_cancelation_policy, null);
        dialog.setContentView(contentView);


        ImageView backButton= contentView.findViewById(R.id.closeicon);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }
}
