package com.ptravels.seatlayout.presentation;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.response.BoardingPoint;

import java.util.ArrayList;

/**
 * Created by praveenkumar on 23/12/17.
 */

public class BoardingPointsFragment extends BottomSheetDialogFragment {

    protected ArrayList<BoardingPoint> boardingPoints = new ArrayList<>();
    private int selectedStationId = -1;

    public void provideData(ArrayList<BoardingPoint> boardingPoints, int selectedId) {
        this.boardingPoints = boardingPoints;
        this.selectedStationId = selectedId;
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_board_deboard, null);
        TextView titleView = contentView.findViewById(R.id.title_dialogue);
        titleView.setText("Select a Boarding Point");

        ImageView backButton = contentView.findViewById(R.id.backBtn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        ListView placesListView = contentView.findViewById(R.id.placesList);
        final PlacesListingAdapter placesListingAdapter = new PlacesListingAdapter(boardingPoints);
        placesListView.setAdapter(placesListingAdapter);
        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((BusMapActivity) getActivity()).setBoardingPoint(boardingPoints.get(i));
                selectedStationId = boardingPoints.get(i).getStationPointId();
                Log.d("clicked", "help");
                placesListingAdapter.notifyDataSetChanged();
                dialog.cancel();
            }
        });
        dialog.setContentView(contentView);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private class PlacesListingAdapter extends BaseAdapter {


        PlacesListingAdapter() {
            super();
        }

        public PlacesListingAdapter(ArrayList<BoardingPoint> boardingPoints) {
            super();
        }

        @Override
        public int getCount() {
            return boardingPoints.size();
        }

        @Override
        public BoardingPoint getItem(int i) {
            return boardingPoints.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View inflate = getLayoutInflater().inflate(R.layout.boarding_deboardingpoints_adaptor,
                    viewGroup, false);
            TextView placeName = inflate.findViewById(R.id.placeName);
            RadioButton radioButton = inflate.findViewById(R.id.placeButton);
            TextView placeTime = inflate.findViewById(R.id.placeTime);

            BoardingPoint boardingPoint = boardingPoints.get(i);
            String stationPointTime = Utils.formatDateString("HH:mm:ss", "h:mm a", boardingPoint.getStationPointTime());
            Integer stationPointId = boardingPoint.getStationPointId();
            String stationPointName = boardingPoint.getStationPointName();

            placeName.setText(stationPointName);
            placeTime.setText(stationPointTime);
            if (stationPointId == selectedStationId) {
                radioButton.setChecked(true);
            } else {
                radioButton.setChecked(false);
            }
            return inflate;
        }
    }
}
