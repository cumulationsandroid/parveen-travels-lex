/*
 * Copyright 2016-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.ptravels.lex;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobileconnectors.lex.interactionkit.Response;
import com.amazonaws.mobileconnectors.lex.interactionkit.config.InteractionConfig;
import com.amazonaws.mobileconnectors.lex.interactionkit.ui.InteractiveVoiceView;
import com.amazonaws.regions.Regions;
import com.amazonaws.util.StringUtils;
import com.ptravels.R;
import com.ptravels.SearchBusses.presentation.SearchResultsActivity;
import com.ptravels.global.Constants;
import com.ptravels.global.Utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class InteractiveVoiceActivity extends Activity
        implements InteractiveVoiceView.InteractiveVoiceListener {
    private static final String TAG = "VoiceActivity";
    private Context appContext;
    private InteractiveVoiceView voiceView;
    private TextView transcriptTextView;
    private TextView responseTextView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interactive_voice);
        transcriptTextView = (TextView) findViewById(R.id.transcriptTextView);
        responseTextView = (TextView) findViewById(R.id.responseTextView);
        init();
        StringUtils.isBlank("notempty");
    }

    @Override
    public void onBackPressed() {
        exit();
        startActivity(new Intent(InteractiveVoiceActivity.this,LexSearchActivity.class));
    }

    private void init() {
        appContext = getApplicationContext();
        voiceView = (InteractiveVoiceView) findViewById(R.id.voiceInterface);
        voiceView.setInteractiveVoiceListener(this);
        CognitoCredentialsProvider credentialsProvider = new CognitoCredentialsProvider(
                /*appContext.getResources().getString(R.string.identity_id_test)*/LexSearchActivity.CognitoIdentityPoolID,
                Regions.fromName(appContext.getResources().getString(R.string.cognito_region)));
        voiceView.getViewAdapter().setCredentialProvider(credentialsProvider);
        voiceView.getViewAdapter().setInteractionConfig(
                new InteractionConfig(appContext.getString(R.string.bot_name),
                        appContext.getString(R.string.bot_alias)));
        voiceView.getViewAdapter().setAwsRegion(appContext.getString(R.string.lex_region));
    }

    private void exit() {
        finish();
    }

    @Override
    public void dialogReadyForFulfillment(final Map<String, String> slots, final String intent) {
        Log.d(TAG, String.format(
                Locale.US,
                "Dialog ready for fulfillment:\n\tIntent: %s\n\tSlots: %s",
                intent,
                slots.toString()));
    }

    @Override
    public void onResponse(Response response) {
        Log.d(TAG, "Bot response: " + response.getTextResponse());
        Log.d(TAG, "Transcript: " + response.getInputTranscript());

        responseTextView.setText(response.getTextResponse());
        transcriptTextView.setText(response.getInputTranscript());

        if (response.getDialogState().equalsIgnoreCase("Fulfilled")){
            goToSearchResultsScreen(response.getSlots());
        }
    }

    @Override
    public void onError(final String responseText, final Exception e) {
        Log.e(TAG, "Error: " + responseText, e);
    }

    private void goToSearchResultsScreen(Map<String, String> slots) {
        String frmCity = slots.get("FromCity");
        String toCity = slots.get("ToCity");
        String travelDate = slots.get("TravelDate");
        String busType = slots.get("BusType");

        HashMap<String,String> stationIds = Utils.getUtils().getStationIdsMap(this);
        if (stationIds == null) {
            Toast.makeText(appContext, "Station Ids not available", Toast.LENGTH_SHORT).show();
            return;
        }

        String frmId = null,toId = null;
        if (frmCity!=null && stationIds.containsKey(frmCity.toLowerCase())){
            frmId = stationIds.get(frmCity.toLowerCase());
        }

        if (toCity!=null && stationIds.containsKey(toCity.toLowerCase())){
            toId = stationIds.get(toCity.toLowerCase());
        }

        startActivity(new Intent(InteractiveVoiceActivity.this, SearchResultsActivity.class)
                .putExtra(Constants.FROM_STATION_ID, frmId)
                .putExtra(Constants.TO_STATION_ID, toId)
                .putExtra(Constants.FROM_STATION_NAME, frmCity)
                .putExtra(Constants.TO_STATION_NAME, toCity)
                .putExtra(Constants.TRAVEL_DATE, travelDate /*yyyy-MM-dd*/)
//                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}
