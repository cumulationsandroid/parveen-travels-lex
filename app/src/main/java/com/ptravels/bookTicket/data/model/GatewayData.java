package com.ptravels.bookTicket.data.model;

/**
 * Created by Amit Tumkur on 05-01-2018.
 */

public class GatewayData {
    private String paymentGatewayProviderName;
    private String paymentGatewayProviderCode;
    private String gatewayModeCode;
    private String paymentGatewayProviderId;
    private String gatewayModeName;
    private String gatewayModeId;

    public String getPaymentGatewayProviderName() {
        return paymentGatewayProviderName;
    }

    public void setPaymentGatewayProviderName(String paymentGatewayProviderName) {
        this.paymentGatewayProviderName = paymentGatewayProviderName;
    }

    public String getPaymentGatewayProviderCode() {
        return paymentGatewayProviderCode;
    }

    public void setPaymentGatewayProviderCode(String paymentGatewayProviderCode) {
        this.paymentGatewayProviderCode = paymentGatewayProviderCode;
    }

    public String getGatewayModeCode() {
        return gatewayModeCode;
    }

    public void setGatewayModeCode(String gatewayModeCode) {
        this.gatewayModeCode = gatewayModeCode;
    }

    public String getPaymentGatewayProviderId() {
        return paymentGatewayProviderId;
    }

    public void setPaymentGatewayProviderId(String paymentGatewayProviderId) {
        this.paymentGatewayProviderId = paymentGatewayProviderId;
    }

    public String getGatewayModeName() {
        return gatewayModeName;
    }

    public void setGatewayModeName(String gatewayModeName) {
        this.gatewayModeName = gatewayModeName;
    }

    public String getGatewayModeId() {
        return gatewayModeId;
    }

    public void setGatewayModeId(String gatewayModeId) {
        this.gatewayModeId = gatewayModeId;
    }
}
