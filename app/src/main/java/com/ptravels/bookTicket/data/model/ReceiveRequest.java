package com.ptravels.bookTicket.data.model;

/**
 * Created by Amit Tumkur on 17-01-2018.
 */

public class ReceiveRequest {
    private String token;
    private String pnr;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }
}
