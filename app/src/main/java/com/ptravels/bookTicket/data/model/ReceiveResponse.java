package com.ptravels.bookTicket.data.model;

public class ReceiveResponse{
	private String merchantreturnurl;
	private boolean success;
	private EnquiryDetails enquiryDetails;
	private String message;
	private boolean error;
	private String transactionId;
	private boolean status;

	public void setMerchantreturnurl(String merchantreturnurl){
		this.merchantreturnurl = merchantreturnurl;
	}

	public String getMerchantreturnurl(){
		return merchantreturnurl;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setEnquiryDetails(EnquiryDetails enquiryDetails){
		this.enquiryDetails = enquiryDetails;
	}

	public EnquiryDetails getEnquiryDetails(){
		return enquiryDetails;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setError(boolean error){
		this.error = error;
	}

	public boolean isError(){
		return error;
	}

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}
}
