package com.ptravels.bookTicket.domain;

import com.ptravels.global.UseCase;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;

import rx.Single;

/**
 * Created by Amit Tumkur on 05-01-2018.
 */

public class PreTransactionUseCase extends UseCase<PreTransactionRequest,PreTransactionResponse> {
    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<PreTransactionResponse> buildUseCase(PreTransactionRequest preTransactionRequest) {
        return bookTicketDataInterface.getPreRedirectResponse(preTransactionRequest);
    }
}
