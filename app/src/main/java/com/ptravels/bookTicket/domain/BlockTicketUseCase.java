package com.ptravels.bookTicket.domain;

import com.ptravels.global.UseCase;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BlockTicketUseCase extends UseCase<BlockTicketRequest,BlockTicketResponse> {

    private BookTicketDataInterface ticketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<BlockTicketResponse> buildUseCase(BlockTicketRequest blockTicketRequest) {
        return ticketDataInterface.blockTicket(blockTicketRequest);
    }
}
